<xc:XmlCache xc:action="Update" xc:output="errors_only" xmlns:cm="mx.MarketParameters.commodity" xmlns:cmfp="mx.MarketParameters.Commodities.Futures.Prices" xmlns:mp="mx.MarketParameters" xmlns:xc="XmlCache">
	<xc:XmlCacheArea xc:value="MarketParameters">
		<mp:nickName xc:value="MDS" xmlns:mp="mx.MarketParameters">
		<#list .data_model?keys as date>
			<mp:date xc:value="${date}">
				<cm:commodity xmlns:cm="mx.MarketParameters.Commodities">
					<cmfp:futurePrice xmlns:cmop="mx.MarketParameters.Commodities.Futures.Prices">
					<#assign typologyMap=.data_model[date]>
					<#assign futuresMap=typologyMap["FUT"]>
					<#list futuresMap?keys as instrument>
						<cmfp:future xc:value="${instrument}">
						<#assign futPrice=futuresMap[instrument]>
							<cmfp:publisher xc:value="${futPrice.publisher}">
								<cmfp:quotation xc:value="${futPrice.quotation}">
								<#assign matList=futPrice.futPrice>
								<#list matList?keys as maturity>
									<cmfp:maturity xc:value="${maturity}">
									<#assign priceList=matList[maturity]>
									<#list priceList as prices>
										<mp:ask>${prices.settlementPrice}</mp:ask>
										<mp:askActive>1</mp:askActive>
										<mp:bid>${prices.settlementPrice}</mp:bid>
										<mp:bidActive>1</mp:bidActive>
										<mp:mid>${prices.settlementPrice}</mp:mid>
									</#list>
									</cmfp:maturity>
								</#list>
								</cmfp:quotation>
							</cmfp:publisher>
						</cmfp:future>
					</#list>
					</cmfp:futurePrice>
				</cm:commodity>
			</mp:date>
		</#list>
		</mp:nickName>
	</xc:XmlCacheArea>
</xc:XmlCache>