<xc:XmlCache xc:action="Update" xmlns:xc="XmlCache">
	<xc:XmlCacheArea xc:value="MarketParameters">
		<mp:nickName xc:value="MDS" xmlns:mp="mx.MarketParameters">
		<#list .data_model?keys as date>
			<mp:date xc:value="${date}">
				<fg:fixing xmlns:fg="mx.MarketParameters.Fixing">
					<fgfp:futurePrice xmlns:fgfp="mx.Fixing.Commodities.Futures">
					<#assign typologyMap=.data_model[date]>
					<#assign futuresMap=typologyMap["FUT"]>
					<#list futuresMap?keys as instrument>
						<fgfp:future xc:value="${instrument}">
						<#assign futPrice=futuresMap[instrument]>
							<fgfp:quotation xc:value="${futPrice.quotation}">
							<#assign matList=futPrice.futPrice>
							<#list matList?keys as maturity>
								<fgfp:maturity xc:value="${maturity}">
								<#assign priceList=matList[maturity]>
								<#list priceList as prices>
									<fgfp:column xc:type="Fields" xc:value="CLOSE">
										<mp:HisValue xc:keyFormat="C">${prices.settlementPrice}</mp:HisValue>
									</fgfp:column>
								</#list>
								</fgfp:maturity>
							</#list>
							</fgfp:quotation>
						</fgfp:future>
					</#list>
					</fgfp:futurePrice>
				</fg:fixing>
			</mp:date>
		</#list>	
		</mp:nickName>
	</xc:XmlCacheArea>
</xc:XmlCache>