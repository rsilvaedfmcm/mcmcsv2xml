<xc:XmlCache xc:action="Insert" xc:output="detailed" xmlns:cm="mx.MarketParameters.commodity" xmlns:cmop="mx.MarketParameters.Commodities.OptionsPrices" xmlns:mp="mx.MarketParameters" xmlns:xc="XmlCache">
	<xc:XmlCacheArea xc:value="MarketParameters">
		<mp:nickName xc:value="MDS" xmlns:mp="mx.MarketParameters">
		<#list .data_model?keys as date>
			<mp:date xc:value="${date}">
				<cm:commodity xmlns:cm="mx.MarketParameters.Commodities">
					<cmop:optionPrice xmlns:cmop="mx.MarketParameters.Commodities.OptionsPrices">
					<#assign typologyMap=.data_model[date]>
					<#assign optionsMap=typologyMap["OOF"]>
					<#list optionsMap?keys as instrument>
						<cmop:option xc:value="${instrument}">
						<#assign optPrice=optionsMap[instrument]>
							<cmop:publisher xc:value="${optPrice.publisher}">
								<cmop:quotation xc:value="${optPrice.quotation}">
								<#assign matList=optPrice.optPrice>
								<#list matList?keys as maturity>
									<cmop:maturity xc:value="${maturity}">
									<#assign priceList=matList[maturity]>
									<#list priceList as prices>
										<cmop:callput xc:value="${prices.callPut}">
											<cmop:strike xc:value="${prices.strike}">
												<mp:ask>${prices.settlementPrice}</mp:ask>
												<mp:askActive>1</mp:askActive>
												<mp:bid>${prices.settlementPrice}</mp:bid>
												<mp:bidActive>1</mp:bidActive>
												<mp:mid>${prices.settlementPrice}</mp:mid>
											</cmop:strike>
										</cmop:callput>
									</#list>
									</cmop:maturity>
								</#list>
								</cmop:quotation>
							</cmop:publisher>
						</cmop:option>
					</#list>
					</cmop:optionPrice>
				</cm:commodity>
			</mp:date>
		</#list>
		</mp:nickName>
	</xc:XmlCacheArea>
</xc:XmlCache>