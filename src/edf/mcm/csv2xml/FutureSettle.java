package edf.mcm.csv2xml;

public class FutureSettle 
{
	private String settlementPrice;
	
	public FutureSettle(String setPrice)
	{
		this.setSettlementPrice(setPrice);
	}

	public String getSettlementPrice() {
		return settlementPrice;
	}

	public void setSettlementPrice(String settlementPrice) {
		this.settlementPrice = settlementPrice;
	}

}
