package edf.mcm.csv2xml;

import com.opencsv.CSVReader;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;


public class Program {

	public static void main(String[] args) {
		
		if(args[0].isEmpty() || args[1].isEmpty() || args[2].isEmpty() || args[3].isEmpty() || args[4].isEmpty() || args[5].isEmpty())
		{
			System.out.println("Please insert all the required parameters.");
			return;
		}
		
		Path configPath = FileSystems.getDefault().getPath(args[1]);
		
		Path configFileName = configPath.getFileName();
		Path configPaths = configPath.getParent();
		
		Path sourcePath = FileSystems.getDefault().getPath(args[3]);
		
		Path destinationPath = FileSystems.getDefault().getPath(args[5]);
		
		System.out.println("=====================================================");
		System.out.println("MCMCSV2XML v1.0 (c) 2018 ED&F Man Capital Markets");
		System.out.println("Contact: Rodrigo Silva - rsilva@edfmancapital.com - U1000D");
		System.out.println("www.edfmancapital.com");
		System.out.println("=====================================================");
		System.out.println("Use schema [" + configPath.toString() +"]");
		System.out.println("Use input file [" + sourcePath.toString() + "]");
		System.out.println("Use ouput file [" + destinationPath.toString() + "]");
		
		
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_27);
		
		try 
		{
			cfg.setDirectoryForTemplateLoading(new File(configPaths.toUri()));
			cfg.setDefaultEncoding("UTF-8");
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			cfg.setLogTemplateExceptions(false);
			cfg.setWrapUncheckedExceptions(true);
			
		} catch (Exception e1) {
			System.out.println("Error - FTL Configuration - main(): " + e1.getMessage());
			return;
		}

		
		try
		{
			System.out.println("Starting file transformation...");
			long startTime = System.currentTimeMillis();
			CSVTree csv = new CSVTree();
			
			Map<String, Map<String, Object>> root = csv.createStructureCsv(sourcePath, ';');
			
			Template temp = cfg.getTemplate(configFileName.toString());

			
			OutputStream outputStream       = new FileOutputStream(destinationPath.toString());
			Writer out = new OutputStreamWriter(outputStream);
			temp.process(root, out);
			
			out.close();
			long endTime = System.currentTimeMillis();
			long timeElapsed = endTime - startTime;
			Date date = new Date(timeElapsed);
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			String formatted = formatter.format(date);
			
			System.out.println("File transformation ended. Total running time: " + formatted);

			
		}
		catch (Exception e)
		{
			System.out.println("Error - main(): " + e.getMessage());
		}
	}

}
