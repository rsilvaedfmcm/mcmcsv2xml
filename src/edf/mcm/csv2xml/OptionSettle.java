package edf.mcm.csv2xml;

public class OptionSettle {

	private String callPut;
	private String strike;
	private String settlementPrice;
	
	public OptionSettle(String callPut, String strike, String settlementPrice)
	{
		this.setCallPut(callPut);
		this.setStrike(strike);
		this.setSettlementPrice(settlementPrice);
	}

	public String getCallPut() {
		return callPut;
	}

	public void setCallPut(String callPut) {
		this.callPut = callPut;
	}

	public String getStrike() {
		return strike;
	}

	public void setStrike(String strike) {
		this.strike = strike;
	}

	public String getSettlementPrice() {
		return settlementPrice;
	}

	public void setSettlementPrice(String settlementPrice) {
		this.settlementPrice = settlementPrice;
	}
	
	
}
