package edf.mcm.csv2xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FuturePrice 
{
	private String publisher;
	private String quotation;
	private Map<String, List<FutureSettle>> futPrice;
	
	public Map<String, List<FutureSettle>> getFutPrice() {
		return futPrice;
	}

	public void setFutPrice(Map<String, List<FutureSettle>> futPrice) {
		this.futPrice = futPrice;
	}

	public FuturePrice(String publisher, String quotation)
	{
		this.setPublisher(publisher);
		this.setQuotation(quotation);
		
		this.futPrice = new HashMap<>();
	}
	
	public void addNewMaturityPrice(String maturity, FutureSettle price)
	{
		if(futPrice.containsKey(maturity))
		{ 
			futPrice.get(maturity).add(price);
		}
		else
		{
			List<FutureSettle> newList = new ArrayList<FutureSettle>();
			
			newList.add(price);
			
			futPrice.put(maturity, newList);
		}
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getQuotation() {
		return quotation;
	}

	public void setQuotation(String quotation) {
		this.quotation = quotation;
	}

}
