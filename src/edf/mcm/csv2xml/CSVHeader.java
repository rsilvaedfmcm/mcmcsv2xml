package edf.mcm.csv2xml;

public class CSVHeader {
	
	public int PUB = -1;
	public int DAT = -1;
	public int REC = -1;
	public int XCH_S = -1;
	public int XCH_T = -1;
	public int CNT_S = -1;
	public int CNT_T = -1;
	public int MTP = -1;
	public int LOT = -1;
	public int FCT = -1;
	public int UND_S = -1;
	public int UND_T = -1;
	public int INS = -1;
	public int QOT_T = -1;
	public int MATU = -1;
	public int MAT_S = -1;
	public int MAT_T = -1;
	public int STK = -1;
	public int OPR = -1;
	public int STL = -1;
	public int VOL = -1;
	
	public CSVHeader(String[] fileHeader)
	{
		for(int i = 0; i < fileHeader.length; i++)
		{
			switch(fileHeader[i]) {
			case "PUB":
				this.PUB = i;
			case "DAT":
				this.DAT = i;
			case "REC":
				this.REC = i;
			case "XCH_S":
				this.XCH_S = i;
			case "XCH_T":
				this.XCH_T = i;
			case "CNT_S":
				this.CNT_S = i;
			case "CNT_T":
				this.CNT_T = i;
			case "MTP":
				this.MTP = i;
			case "LOT":
				this.LOT = i;
			case "FCT":
				this.FCT = i;
			case "UND_S":
				this.UND_S = i;
			case "UND_T":
				this.UND_T = i;
			case "INS":
				this.INS = i;
			case "QOT_T":
				this.QOT_T = i;
			case "MATU":
				this.MATU = i;
			case "MAT_S":
				this.MAT_S = i;
			case "MAT_T":
				this.MAT_T = i;
			case "STK":
				this.STK = i;
			case "OPR":
				this.OPR = i;
			case "STL":
				this.STL = i;
			case "VOL":
				this.VOL = i;
				
			}
		}
	}

}
