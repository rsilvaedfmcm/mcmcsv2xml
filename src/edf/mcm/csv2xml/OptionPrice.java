package edf.mcm.csv2xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OptionPrice 
{
	private String publisher;
	private String quotation;
	private Map<String, List<OptionSettle>> optPrice;
	
	public Map<String, List<OptionSettle>> getOptPrice() {
		return optPrice;
	}

	public void setOptPrice(Map<String, List<OptionSettle>> optPrice) {
		this.optPrice = optPrice;
	}

	public OptionPrice(String publisher, String quotation)
	{
		this.setPublisher(publisher);
		this.setQuotation(quotation);
		
		this.optPrice = new HashMap<>();
	}
	
	public void addNewMaturityPrice(String maturity, OptionSettle price)
	{
		if(optPrice.containsKey(maturity))
		{ 
			optPrice.get(maturity).add(price);
		}
		else
		{
			List<OptionSettle> newList = new ArrayList<OptionSettle>();
			
			newList.add(price);
			
			optPrice.put(maturity, newList);
		}
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getQuotation() {
		return quotation;
	}

	public void setQuotation(String quotation) {
		this.quotation = quotation;
	}
}
