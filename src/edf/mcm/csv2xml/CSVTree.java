package edf.mcm.csv2xml;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.io.StringWriter;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

public class CSVTree {
	
	private Map<String, Map<String, Object>> csvRoot;
	private Map<String, Object> typologyMap;
	
	private Map<String, FuturePrice> futuresMap;
	private Map<String, OptionPrice> optionsMap;
	
	private CSVReader csv;
	private Reader reader;
	private CSVHeader oHeader;
	
	public CSVTree()
	{
		csvRoot = new HashMap<>();
		typologyMap = new HashMap<>();
		
		futuresMap = new HashMap<>();
		optionsMap = new HashMap<>();
		
	}
	
		
	public Map<String, Map<String, Object>> createStructureCsv(Path sourceFile, char separator)
	{
		
		try {
			reader = Files.newBufferedReader(sourceFile, StandardCharsets.UTF_8);
			
			
			CSVParser parser = new CSVParserBuilder().withSeparator(separator).build();
			CSVReader csv = new CSVReaderBuilder(reader).withCSVParser(parser).build();
			
			String[] header = csv.readNext();
			
			oHeader = new CSVHeader(header);
			
			String[] nextRecord;
			while( (nextRecord = csv.readNext()) != null )
			{
				String date = nextRecord[oHeader.DAT];
				String typo = nextRecord[oHeader.INS];
				String instrument = nextRecord[oHeader.CNT_T];
				String maturity = nextRecord[oHeader.MAT_T];
				String price = nextRecord[oHeader.STL];
				String callPut = nextRecord[oHeader.OPR];
				String strike = nextRecord[oHeader.STK];
				
				
				if(csvRoot.containsKey(date))
				{
					if(typo.equals("FUT"))
					{
						Map<String, Object> fMap = csvRoot.get(date);
						
						Map<String, FuturePrice> fPrices = (Map<String, FuturePrice>) fMap.get(typo);
						
						if(fPrices.containsKey(instrument))
						{
							fPrices.get(instrument).addNewMaturityPrice(maturity, new FutureSettle(price));
						}
						else
						{
							String publisher = nextRecord[oHeader.XCH_T];
							String quotation = nextRecord[oHeader.QOT_T];
							
							FuturePrice fprice = new FuturePrice(publisher, quotation);
							
							fprice.addNewMaturityPrice(maturity, new FutureSettle(price));
							
							fPrices.put(instrument, fprice);
						}
					}
					else if(typo.equals("OOF"))
					{
						Map<String, Object> fMap = csvRoot.get(date);
						
						Map<String, OptionPrice> oPrices = (Map<String, OptionPrice>) fMap.get(typo);
						
						if(oPrices.containsKey(instrument))
						{
							oPrices.get(instrument).addNewMaturityPrice(maturity, new OptionSettle(callPut, strike, price));
						}
						else
						{
							String publisher = nextRecord[oHeader.XCH_T];
							String quotation = nextRecord[oHeader.QOT_T];
							
							OptionPrice oprice = new OptionPrice(publisher, quotation);
							
							oprice.addNewMaturityPrice(maturity, new OptionSettle(callPut, strike, price));
							
							oPrices.put(instrument, oprice);
						}
					}
						
				}
				else
				{
					createFirstRegister(nextRecord);
				}
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return csvRoot;
	}
	
	private void createFirstRegister(String[] nextRecord)
	{
		String date = nextRecord[oHeader.DAT];
		String typo = nextRecord[oHeader.INS];
		String instrument = nextRecord[oHeader.CNT_T];
		String maturity = nextRecord[oHeader.MAT_T];
		String publisher = nextRecord[oHeader.XCH_T];
		String quotation = nextRecord[oHeader.QOT_T];
		String price = nextRecord[oHeader.STL];
		String callPut = nextRecord[oHeader.OPR];
		String strike = nextRecord[oHeader.STK];
		
		Map<String, Object> typoMap = new HashMap<>();
		
		Map<String, FuturePrice> futMap = new HashMap<>();
		Map<String, OptionPrice> optMap = new HashMap<>();
		
		typoMap.put("FUT", futMap);
		typoMap.put("OOF", optMap);
		
		csvRoot.put(date, typoMap);
		
		if(typo.equals("FUT"))
		{
			Map<String, Object> fMap = csvRoot.get(date);
			
			Map<String, FuturePrice> fPrices = (Map<String, FuturePrice>) fMap.get(typo);
			
			FuturePrice fut = new FuturePrice(publisher, quotation);
			
			fut.addNewMaturityPrice(maturity, new FutureSettle(price));
			
			fPrices.put(instrument, fut);
		}
		else if(typo.equals("OOF"))
		{
			Map<String, Object> fMap = csvRoot.get(date);
			
			Map<String, OptionPrice> oPrices = (Map<String, OptionPrice>) fMap.get(typo);
			
			OptionPrice opt = new OptionPrice(publisher, quotation);
			
			opt.addNewMaturityPrice(maturity, new OptionSettle(callPut, strike,price));
			
			oPrices.put(instrument, opt);
		}
	}

}
